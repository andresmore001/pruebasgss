﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using PruebasGssCore.Exceptions;
using PruebasGssInfraestructure.Exceptions;


namespace PruebasGssInfraestructure.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(BusinessException))
            {
                var excepcion = (BusinessException)context.Exception;
                var validation = new
                {
                    code = 400,
                    message = "Bad Request",
                    innerError = excepcion.Message
                };
                var json = new
                {
                    errors = new[] { validation }
                };
                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.ExceptionHandled = true;
            }
            if (context.Exception.GetType() == typeof(InfraestructureException))
            {
                var excepcion = (InfraestructureException)context.Exception;
                var validation = new
                {
                    code = 500,
                    message = "Internal Server Error",
                    innerError = excepcion.Message
                };
                var json = new
                {
                    errors = new[] { validation }
                };
                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.ExceptionHandled = true;
            }
        
        }
    }
}
