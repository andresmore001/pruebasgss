﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebasGssInfraestructure.Exceptions
{
    public class InfraestructureException : Exception
    {
        public InfraestructureException()
        {

        }
        public InfraestructureException(string message) : base(message)
        {

        }
    }
}
