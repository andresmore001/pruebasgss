﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PruebasGssCore.Entities;

namespace PruebasGssInfraestructure.Data
{
    public partial class PRUEBAGSSContext : DbContext
    {
        public PRUEBAGSSContext()
        {
        }

        public PRUEBAGSSContext(DbContextOptions<PRUEBAGSSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alquiler> Alquiler { get; set; }
        public virtual DbSet<Carro> Carro { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Pagos> Pagos { get; set; }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alquiler>(entity =>
            {
                entity.HasKey(e => e.AlIdalquiler)
                    .HasName("PK__ALQUILER__D8E09C98125ACB07");

                entity.ToTable("ALQUILER");

                entity.Property(e => e.AlIdalquiler).HasColumnName("AL_IDALQUILER");

                entity.Property(e => e.AlAbonoinicial)
                    .HasColumnName("AL_ABONOINICIAL")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.AlDevuelto)
                    .HasColumnName("AL_DEVUELTO")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AlFecha)
                    .HasColumnName("AL_FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.AlIdcarro).HasColumnName("AL_IDCARRO");

                entity.Property(e => e.AlIdcliente).HasColumnName("AL_IDCLIENTE");

                entity.Property(e => e.AlSaldo)
                    .HasColumnName("AL_SALDO")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.AlTiempo).HasColumnName("AL_TIEMPO");

                entity.HasOne(d => d.AlIdcarroNavigation)
                    .WithMany(p => p.Alquiler)
                    .HasForeignKey(d => d.AlIdcarro)
                    .HasConstraintName("FK__ALQUILER__AL_IDC__2F10007B");

                entity.HasOne(d => d.AlIdclienteNavigation)
                    .WithMany(p => p.Alquiler)
                    .HasForeignKey(d => d.AlIdcliente)
                    .HasConstraintName("FK__ALQUILER__AL_IDC__300424B4");
            });

            modelBuilder.Entity<Carro>(entity =>
            {
                entity.HasKey(e => e.CarIdcarro)
                    .HasName("PK__CARRO__56F84D7F6342A2B6");

                entity.ToTable("CARRO");

                entity.HasIndex(e => e.CarPlaca)
                    .HasName("UNIQUE_PLACA")
                    .IsUnique();

                entity.Property(e => e.CarIdcarro).HasColumnName("CAR_IDCARRO");

                entity.Property(e => e.CarCosto)
                    .HasColumnName("CAR_COSTO")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.CarDisponible)
                    .HasColumnName("CAR_DISPONIBLE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CarMarca)
                    .IsRequired()
                    .HasColumnName("CAR_MARCA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CarModelo)
                    .IsRequired()
                    .HasColumnName("CAR_MODELO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CarPlaca)
                    .IsRequired()
                    .HasColumnName("CAR_PLACA")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.CliIdcliente)
                    .HasName("PK__CLIENTE__A45EE0680903CBDF");

                entity.ToTable("CLIENTE");

                entity.HasIndex(e => e.CliCedula)
                    .HasName("UNIQUE_CEDULA")
                    .IsUnique();

                entity.HasIndex(e => e.CliTelefono1)
                    .HasName("UNIQUE_TEL1")
                    .IsUnique();

                entity.HasIndex(e => e.CliTelefono2)
                    .HasName("UNIQUE_TEL2")
                    .IsUnique();

                entity.Property(e => e.CliIdcliente).HasColumnName("CLI_IDCLIENTE");

                entity.Property(e => e.CliCedula)
                    .IsRequired()
                    .HasColumnName("CLI_CEDULA")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CliNombre)
                    .IsRequired()
                    .HasColumnName("CLI_NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CliTelefono1)
                    .IsRequired()
                    .HasColumnName("CLI_TELEFONO1")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CliTelefono2)
                    .HasColumnName("CLI_TELEFONO2")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<Pagos>(entity =>
            {
                entity.HasKey(e => e.PgIdpago)
                    .HasName("PK__PAGOS__A21F5E50D982F2A6");

                entity.ToTable("PAGOS");

                entity.Property(e => e.PgIdpago).HasColumnName("PG_IDPAGO");

                entity.Property(e => e.PgFecha)
                    .HasColumnName("PG_FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.PgIdalquiler).HasColumnName("PG_IDALQUILER");

                entity.Property(e => e.PgValor)
                    .HasColumnName("PG_VALOR")
                    .HasColumnType("numeric(18, 2)");

                entity.HasOne(d => d.PgIdalquilerNavigation)
                    .WithMany(p => p.Pagos)
                    .HasForeignKey(d => d.PgIdalquiler)
                    .HasConstraintName("FK__PAGOS__PG_IDALQU__32E0915F");
            });

        }

    }
}
