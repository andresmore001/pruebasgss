﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using PruebasGssCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPruebasGss.Controllers
{
    [EnableCors("Policy")]
    [Route("api/[controller]")]
    [ApiController]
    public class VistaDosController : ControllerBase
    {
        private IConfiguration _Configuration { get; }
        public VistaDosController(IConfiguration Configuration)
        {
            _Configuration = Configuration;
        }
        [HttpGet]
        public async Task<List<vistaDos>> GetInfo(string Fecha)
        {
            using (SqlConnection sql = new SqlConnection(_Configuration.GetConnectionString("conection")))
            {
                using (SqlCommand cmd = new SqlCommand("VistaDos", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@FECHA", Fecha));
                    var response = new List<vistaDos>();
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response.Add(MapToValue(reader));
                        }
                    }

                    return response;
                }
            }
        }
        private vistaDos MapToValue(SqlDataReader reader)
        {
            return new vistaDos()
            {
                ALQUILERMES=(int) reader["ALQUILERMES"]
            };
        }

    }
}
