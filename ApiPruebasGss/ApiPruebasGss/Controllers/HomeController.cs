﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using PruebasGssCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPruebasGss.Controllers
{
    [EnableCors("Policy")]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private IConfiguration _Configuration { get; }
        public HomeController(IConfiguration Configuration)
        {
            _Configuration = Configuration;
        }
        [HttpGet]
        public async Task<List<Vistauno>> GetInfo(string FechaIni, string FechaFin)
        {
            using (SqlConnection sql = new SqlConnection(_Configuration.GetConnectionString("conection")))
            {
                using (SqlCommand cmd = new SqlCommand("DataVistaUno", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@FechaInicial", FechaIni));
                    cmd.Parameters.Add(new SqlParameter("@FechaFinal", FechaFin));
                    var response = new List<Vistauno>();
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response.Add(MapToValue(reader));
                        }
                    }

                    return response;
                }
            }
        }
        private Vistauno MapToValue(SqlDataReader reader)
        {
            return new Vistauno()
            {
                CEDULA = reader["CEDULA"].ToString(),
                NOMBRE = reader["NOMBRE"].ToString(),
                FECHA_ALQUILER=(DateTime) reader["FECHA_ALQUILER"],
                TIEMPO_ALQUILADO=(int) reader["TIEMPO_ALQUILADO"],
                SALDO=(Decimal)reader["SALDO"],
                PLACA=reader["PLACA"].ToString(),
                MARCA=reader["MARCA"].ToString()
            };
        }

    } 
}
