﻿using System;
using System.Collections.Generic;

namespace PruebasGssCore.Entities
{
    public partial class Cliente
    {
        public Cliente()
        {
            Alquiler = new HashSet<Alquiler>();
        }

        public int CliIdcliente { get; set; }
        public string CliCedula { get; set; }
        public string CliNombre { get; set; }
        public string CliTelefono1 { get; set; }
        public string CliTelefono2 { get; set; }

        public virtual ICollection<Alquiler> Alquiler { get; set; }
    }
}
