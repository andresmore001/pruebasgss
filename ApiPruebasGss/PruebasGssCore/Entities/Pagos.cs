﻿using System;
using System.Collections.Generic;

namespace PruebasGssCore.Entities
{
    public partial class Pagos
    {
        public int PgIdpago { get; set; }
        public DateTime PgFecha { get; set; }
        public decimal PgValor { get; set; }
        public int? PgIdalquiler { get; set; }

        public virtual Alquiler PgIdalquilerNavigation { get; set; }
    }
}
