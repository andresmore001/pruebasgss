﻿using System;
using System.Collections.Generic;

namespace PruebasGssCore.Entities
{
    public partial class Carro
    {
        public Carro()
        {
            Alquiler = new HashSet<Alquiler>();
        }

        public int CarIdcarro { get; set; }
        public string CarPlaca { get; set; }
        public string CarMarca { get; set; }
        public string CarModelo { get; set; }
        public decimal CarCosto { get; set; }
        public bool? CarDisponible { get; set; }

        public virtual ICollection<Alquiler> Alquiler { get; set; }
    }
}
