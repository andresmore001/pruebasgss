﻿using System;
using System.Collections.Generic;

namespace PruebasGssCore.Entities
{
    public partial class Alquiler
    {
        public Alquiler()
        {
            Pagos = new HashSet<Pagos>();
        }

        public int AlIdalquiler { get; set; }
        public DateTime AlFecha { get; set; }
        public int AlTiempo { get; set; }
        public decimal AlSaldo { get; set; }
        public decimal AlAbonoinicial { get; set; }
        public bool? AlDevuelto { get; set; }
        public int? AlIdcarro { get; set; }
        public int? AlIdcliente { get; set; }

        public virtual Carro AlIdcarroNavigation { get; set; }
        public virtual Cliente AlIdclienteNavigation { get; set; }
        public virtual ICollection<Pagos> Pagos { get; set; }
    }
}
