import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PagesRoutingModule } from './pages/pages.routing';

const routes: Routes = [
 {path:'', redirectTo: '/PruebasGss/VistaUno',pathMatch:'full'},
 {path:'**', redirectTo: '/PruebasGss/VistaUno',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ,PagesRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }