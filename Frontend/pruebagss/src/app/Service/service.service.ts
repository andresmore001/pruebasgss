import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  datos: any;
  headers: HttpHeaders = new HttpHeaders({
  });
  constructor(private http: HttpClient) { }

  public Information(param1: number, param2?: any, param3?: any,param4?: any): Observable<any> {
    this.datos = [];
    switch (param1) {
      case 1:
        this.datos = this.http.get(environment.apiLocal + param2, {
          params: {
            FechaIni: param3,
            FechaFin: param4
          }
        });
        break;
      case 2:
          this.datos = this.http.get(environment.apiLocal + param2, {
            params: {
              Fecha: param3,
            }
          });
          break;
        default:
          return this.datos;
          break;
    }
    return this.datos;
  }
}
