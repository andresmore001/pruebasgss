import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-vista-dos',
  templateUrl: './vista-dos.component.html',
  styleUrls: ['./vista-dos.component.css']
})
export class VistaDosComponent implements OnInit {
  ano = new Date().getUTCFullYear();
  mes=new Date().getMonth();
  dia=new Date().getDate();
  datos: Array<any> = new Array<any>();
  progreso: number = 10;
  constructor(private servicio: ServiceService) { }

  ngOnInit(): void {
    this.getInfo();
    setInterval(() => {
      this.getInfo(); 
    }, 60000);
  }
 
  getInfo() {
    console.log("Ejecucion correcta")
    let fecha;
    if (this.mes<10)
    {
      if (this.dia<10)
      {
        fecha=this.ano+'0'+(this.mes+1)+'0'+this.dia
      }else{
         fecha=this.ano+'0'+(this.mes+1)+''+this.dia
      }         
    }
    else{
      if (this.dia<10)
      {
         fecha=this.ano+''+(this.mes+1)+'0'+this.dia
      }else{
         fecha=this.ano+''+(this.mes+1)+''+this.dia
      } 
    }
    this.servicio.Information(2, 'VistaDos',fecha).subscribe(data => {
      this.datos=data;
      console.log(this.datos)
    },(error)=>{
      Swal.fire({
        title: 'error',
        text: 'Ha ocurrido un error obteniendo los datos.',
        icon: 'warning',
      })
    })
  }
    getProgreso(data:any) {
      return `${ data*2 }%`;
    }
}
