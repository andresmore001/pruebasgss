import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VistaUnoComponent } from './vista-uno/vista-uno.component';
import { VistaDosComponent } from './vista-dos/vista-dos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesComponent } from './pages.component';
import { AppRoutingModule } from '../app-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    VistaUnoComponent,
    VistaDosComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports:[
    VistaUnoComponent,
    VistaDosComponent,
    PagesComponent
  ]
})
export class PagesModule { }
