import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { VistaUnoComponent } from './vista-uno/vista-uno.component';
import { VistaDosComponent } from './vista-dos/vista-dos.component';



const Childroutes: Routes = [
    { path: 'VistaUno', component: VistaUnoComponent},
    { path: 'VistaDos', component: VistaDosComponent},
    
];

@NgModule({
    imports: [RouterModule.forChild(Childroutes)],
    exports: [RouterModule]
})
export class ChildRoutesModule { }