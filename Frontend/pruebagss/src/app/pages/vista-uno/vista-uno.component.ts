import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/Service/service.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-vista-uno',
  templateUrl: './vista-uno.component.html',
  styleUrls: ['./vista-uno.component.css']
})
export class VistaUnoComponent implements OnInit {
  formulario: FormGroup;
  title = 'pruebagss';
  tabla: Array<any> = new Array<any>();
  tableIs: boolean=false;
  optionsTable = {
    pagingType: 'simple_numbers',
    pageLength: 5,
    lengthMenu: [5, 10, 15],
    language: environment.language,
    info: false
  };
  constructor(private fb: FormBuilder,private servicio: ServiceService){

  }
  ngOnInit(): void {
    this.ConstruirFormulario();
  }
  ConstruirFormulario(){
    this.formulario=this.fb.group({
       FechaIni:['',Validators.required],
       FechaFin:['',Validators.required],
    })
  }
  getInfo() {
    let fechaini= this.formulario.controls.FechaIni.value;
    let fechafin= this.formulario.controls.FechaFin.value;
    if(!fechaini || !fechafin){
      Swal.fire({
        title: 'error',
        text: 'Debe seleccionar ambas fechas',
        icon: 'warning',
      })
    }else{
      if(fechaini>fechafin){
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
    
        Toast.fire({
          icon: 'error',
          title: 'La fecha inicial no puede ser mayor a la final'
        })
      }else{
        for (let index = 0; index < 2; index++) {
          fechaini=fechaini.replace("-","");
          fechafin=fechafin.replace("-","");
        }
        this.servicio.Information(1, 'home',fechaini,fechafin).subscribe(data => {
          this.tableIs=true;
          this.tabla=data;
          //llama para render de datatable
          this.rerender('TablaAlquileres');
        },(error)=>{
          Swal.fire({
            title: 'error',
            text: 'Ha ocurrido un error obteniendo los datos.',
            icon: 'warning',
          })
        })
      }
      
    }
   
  }
  rerender(idTable1: string) {
    //destruyo tabla antes de reinicializar
    let tabler = $('#' + idTable1).DataTable()
    tabler.destroy();
    setTimeout(() => {
      $('#' + idTable1).DataTable(this.optionsTable);
    }, 300);
  }
}
